package de.dataport.tz41.jamoni.collector.model;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class Configuration {
	
	private String host;
	private int port;
	private int duration;
	private int interval;
	private String dataPrefix;
	
	public Configuration(String host, int port, int duration, int interval, String dataPrefix) {
		this.host = host;
		this.port = port;
		this.duration = duration;
		this.interval = interval;
		this.dataPrefix = dataPrefix;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getDataPrefix() {
		return dataPrefix;
	}

	public void setDataPrefix(String dataPrefix) {
		this.dataPrefix = dataPrefix;
	}
}
