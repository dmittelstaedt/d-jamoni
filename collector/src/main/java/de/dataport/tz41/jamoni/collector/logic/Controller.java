package de.dataport.tz41.jamoni.collector.logic;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.dataport.tz41.jamoni.collector.model.Configuration;
import de.dataport.tz41.jamoni.collector.model.ProcessValues;
import de.dataport.tz41.jamoni.collector.utils.ConfigurationParser;
import de.dataport.tz41.jamoni.collector.utils.FileUtils;
import de.dataport.tz41.jamoni.collector.view.CSVWriter;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class Controller {

	private static final Logger logger = LogManager.getLogger(Controller.class);
	private static final String defaultConfigurationFile = "custom/jamoni.properties";

	private String configurationFile;
	private ProcessValues processValues;
	private Monitor monitor;
	private Configuration configuration;

	public Controller() {
		this.configurationFile = System.getProperty("user.dir") + "/" + defaultConfigurationFile;
		this.processValues = new ProcessValues();
	}

	private void startMonitoring() throws IOException {
		int count = 0;
		boolean isReconnect = false;
		while (count < this.configuration.getDuration() || this.configuration.getDuration() == 0) {
			if (this.monitor.isConnected()) {
				if (isReconnect) {
					this.monitor.logHostInformation();
					this.monitor.logClassPath();
					this.monitor.logMemory("init");
					this.monitor.logMemory("max");
					isReconnect = false;
				}
				this.monitor.logMemory("used");
			} else {
				try {
					this.monitor.createConnection();
					isReconnect = true;
				} catch (IOException e) {
					logger.error("Error while connecting to target: " + e.getMessage());
					logger.debug("Error while connecting to target:", e);
				}
			}
			try {
				Thread.sleep(this.configuration.getInterval()*1000);
			} catch (InterruptedException e) {
				logger.debug("Runtime error occured.", e);
			}
			count++;
		}
	}

	public void start() {
		System.out.println("Current working directory: " + System.getProperty("user.dir"));
		System.out.println("Loading properties from " + this.configurationFile);
		ConfigurationParser configurationParser = new ConfigurationParser(this.configurationFile);
		try {
			this.configuration = configurationParser.load();
		} catch (IOException ioException) {
			logger.error("Could not load properties file.");
			System.out.println("Could not load properties file.");
			System.exit(1);
		}
		logger.debug("Configuration successfully loaded.");
		logger.debug("Check if directory for data file exists...");
		try {
			String absolutePath = FileUtils.getDirectoryFromPath(this.configuration.getDataPrefix());
			if (!FileUtils.isDirectory(absolutePath)) {
				logger.error("Directory " + absolutePath + " for data file does not exist. Check data.prefix in jamoni.properties.");
				logger.debug("Directory " + absolutePath + " for data file does not exist. Check data.prefix in jamoni.properties.");
				System.exit(1);
			}
		} catch (IOException e) {
			logger.error("Error while checking if directory for data file exists: " + e.getMessage());
			logger.debug("Error while checking if directory for data file exists: ", e);
			System.exit(1);
		}
		logger.trace("Directory for data file exists.");
		this.monitor = new Monitor(this.configuration.getHost(), this.configuration.getPort(), this.processValues);
		CSVWriter csvWriter = new CSVWriter(this.configuration.getDataPrefix(), this.processValues);
		this.processValues.addPropertyChangeListener(csvWriter);
		logger.info("Starting monitoring...");
		try {
			startMonitoring();
		} catch (IOException e) {
			logger.error("Monitoring aborted with failures: " + e);
			logger.debug("Monitoring aborted with failures.", e);
			System.exit(1);
		}
		logger.info("Monitoring finished.");
	}
}
