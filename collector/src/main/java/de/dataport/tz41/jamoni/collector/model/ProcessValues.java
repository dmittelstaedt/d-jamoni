package de.dataport.tz41.jamoni.collector.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.dataport.tz41.jamoni.collector.utils.FileUtils;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class ProcessValues {
	
	private static final Logger logger = LogManager.getLogger(ProcessValues.class);

	private long heap;
	private Date date;
	private String type;
	private String classPath;
	private int pid;
	private String hostname;

	private PropertyChangeSupport support;

	public ProcessValues() {
		this.support = new PropertyChangeSupport(this);
	}

	public long getHeap() {
		return heap;
	}

	private void setHeap(long heap) {
		this.heap = heap;
	}

	public Date getDate() {
		return date;
	}

	private void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	private void setType(String type) {
		this.type = type;
	}

	public String getClassPath() {
		return classPath;
	}

	private void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	public int getPid() {
		return pid;
	}

	private void setPid(int pid) {
		this.pid = pid;
	}

	public String getHostname() {
		return hostname;
	}

	private void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public void update(long heap) {
		Date date = Calendar.getInstance().getTime();
		setHeap(heap);
		support.firePropertyChange("date", this.date, date);
		setDate(date);
	}

	public void update(long heap, String type) {
		Date date = Calendar.getInstance().getTime();
		setHeap(heap);
		setType(type);
		logger.debug(this.type.substring(0, 1).toUpperCase() + this.type.substring(1) + " Memory: " + this.heap);
		support.firePropertyChange("date", this.date, date);
		setDate(date);
	}
	
	public void updateClassPath(String classPath) throws IOException {
		Date date = Calendar.getInstance().getTime();
		setDate(date);
		setType("classpath");
		logger.debug("ClassPath: " + FileUtils.getDirectoryFromPath(classPath));
		support.firePropertyChange("classPath", this.classPath, classPath);
		setClassPath(classPath);
	}
	
	public void updatePid(int pid) throws IOException {
		Date date = Calendar.getInstance().getTime();
		setDate(date);
		setType("pid");
		logger.debug("PID: " + pid);
		support.firePropertyChange("pid", this.pid, pid);
		setPid(pid);;
	}
	
	public void updateHostname(String hostname) throws IOException {
		Date date = Calendar.getInstance().getTime();
		setDate(date);
		setType("hostname");
		logger.debug("Hostname: " + hostname);
		support.firePropertyChange("hostname", this.hostname, hostname);
		setHostname(hostname);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		this.support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		this.support.removePropertyChangeListener(pcl);
	}

}
