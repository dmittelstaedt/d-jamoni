package de.dataport.tz41.jamoni.collector.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.dataport.tz41.jamoni.collector.model.Configuration;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class ConfigurationParser {
	
	private final static Logger logger = LogManager.getLogger(ConfigurationParser.class); 

	private final String host = "target.host";
	private final String port = "target.port";
	private final String duration = "duration";
	private final String interval = "interval";
	private final String dataPrefix = "data.prefix";

	private Properties properties;
	private String configurationFile;
	private FileInputStream fileInputStream;
	private Configuration configuration;

	public ConfigurationParser(String configurationFile) {
		this.properties = new Properties();
		this.configurationFile = configurationFile;
	}

	public Configuration load() throws IOException {
		try {
			fileInputStream = new FileInputStream(this.configurationFile);
			properties.load(fileInputStream);
			configuration = new Configuration(properties.getProperty(this.host, "127.0.0.1"),
					Integer.parseInt(properties.getProperty(this.port, "9990")),
					Integer.parseInt(properties.getProperty(this.duration, "0")),
					Integer.parseInt(properties.getProperty(this.interval, "60")),
					properties.getProperty(this.dataPrefix, "jamoni"));
		} catch (IOException e) {
			logger.error("Error while loading the configuration file: " + e.getMessage());
			logger.debug("Error while loadig the configuration file:", e);
			throw e;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					logger.error("Error while closing opened configuration file: " + e.getMessage());
					logger.debug("Error while closing opened configuration file: ", e);
					throw e;
				}
			}
		}
		return configuration;
	}

}
