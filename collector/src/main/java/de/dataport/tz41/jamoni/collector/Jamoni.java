package de.dataport.tz41.jamoni.collector;

import de.dataport.tz41.jamoni.collector.logic.Controller;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class Jamoni {

	public static void main(String[] args) {
		Controller controller = new Controller();
		controller.start();
	}
}
