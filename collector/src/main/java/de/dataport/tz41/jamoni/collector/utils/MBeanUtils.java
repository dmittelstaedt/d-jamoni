package de.dataport.tz41.jamoni.collector.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class MBeanUtils {

	private static final Logger logger = LogManager.getLogger(MBeanUtils.class);

	public static void logCompositeKeys(JMXConnector jmxConnector) {
		try {
			Object memoryMbean = jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
			CompositeData compositeData = (CompositeData) memoryMbean;
			Set<String> keys = compositeData.getCompositeType().keySet();
			for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
				String key = iterator.next();
				logger.info(key);
			}
			logger.info(compositeData.get("init"));
			logger.info(compositeData.get("used"));
			logger.info(compositeData.get("max"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void logAttributes(JMXConnector jmxConnector) {
		try {
			MBeanInfo mBeanInfo = jmxConnector.getMBeanServerConnection().getMBeanInfo(new ObjectName("java.lang:type=Runtime"));
			MBeanAttributeInfo[] infos = mBeanInfo.getAttributes();
			for (int i = 0; i < infos.length; i++) {
				logger.info(infos[i].getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void logBeans(JMXConnector jmxConnector) {
		Set<ObjectName> mBeans;
		try {
			mBeans = jmxConnector.getMBeanServerConnection().queryNames(null, null);
			for (Iterator<ObjectName> iterator = mBeans.iterator(); iterator.hasNext();) {
				Object object = (Object) iterator.next();
				logger.info(object.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
