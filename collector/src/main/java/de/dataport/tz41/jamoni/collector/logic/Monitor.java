package de.dataport.tz41.jamoni.collector.logic;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.dataport.tz41.jamoni.collector.model.ProcessValues;
import de.dataport.tz41.jamoni.collector.utils.FileUtils;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class Monitor {

	private static final Logger logger = LogManager.getLogger(Monitor.class);

	private JMXServiceURL serviceURL;
	private JMXConnector jmxConnector;
	private String host;
	private int port;
	private ProcessValues processValues;
	private String connectionId;

	public Monitor(String host, int port, ProcessValues processValues) {
		this.host = host;
		this.port = port;
		this.processValues = processValues;
		this.connectionId = "";
		initialize();
	}

	private void initialize() {
		try {
			this.serviceURL = new JMXServiceURL("service:jmx:http-remoting-jmx://" + this.host + ":" + this.port);
		} catch (MalformedURLException e) {
			logger.error("Error while creating connection URL: " + e.getMessage());
			logger.debug("Error while creating connection URL:", e);
		}
	}

	private CompositeData getMemoryMBean() {
		Object memoryMbean = null;
		//			jmxc.getMBeanServerConnection().invoke(new ObjectName("java.lang:type=Memory"), "gc", null, null);
		try {
			memoryMbean = this.jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
		} catch (AttributeNotFoundException | InstanceNotFoundException | MalformedObjectNameException | MBeanException
				| ReflectionException | IOException e) {
			logger.error("Error while retrieving memory information: " + e.getMessage());
			logger.debug("Error while retrieving memory information: ", e);
			this.connectionId = "";
		}
		return (CompositeData) memoryMbean;
	}

	public void createConnection() throws IOException {
		logger.debug("Connecting to: " + this.host + ":" + this.port);
		this.serviceURL = new JMXServiceURL("service:jmx:http-remoting-jmx://" + this.host + ":" + this.port);
		//			serviceURL = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi");
		this.jmxConnector = JMXConnectorFactory.connect(this.serviceURL, null);
		this.connectionId = jmxConnector.getConnectionId();
	}

	public boolean isConnected() throws IOException {
		if (this.connectionId.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void logMemory(String typeOfMemory) {
		long memory = -1;
		try {
			memory = Long.parseLong(getMemoryMBean().get(typeOfMemory).toString());
		} catch (NullPointerException e) {
			logger.error("Error while parsing memory information: " + e.getMessage());
			logger.debug("Error while parsing memory information: ", e);
		} finally {
			this.processValues.update(memory, typeOfMemory);
		}
	}

	public void logClassPath() throws IOException {
		String classPath = "";
		try {
			Object classPathMBean = this.jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("java.lang:type=Runtime"), "ClassPath");
			classPath = (String) classPathMBean;
		} catch (AttributeNotFoundException | InstanceNotFoundException | MalformedObjectNameException
				| MBeanException | ReflectionException | IOException e) {
			logger.error("Error while retrieving classpath information: " + e.getMessage());
			logger.debug("Error while retrieving classpath information: ", e);
		} finally {
			this.processValues.updateClassPath(FileUtils.getDirectoryFromPath(classPath));
		}
	}

	public void logHostInformation() throws IOException {
		String hostInformation = "";
		try {
			Object hostInformationMBean = this.jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("java.lang:type=Runtime"), "Name");
			hostInformation = (String) hostInformationMBean;
		} catch (AttributeNotFoundException | InstanceNotFoundException | MalformedObjectNameException
				| MBeanException | ReflectionException | IOException e) {
			logger.error("Error while retrieving host information: " + e.getMessage());
			logger.debug("Error while retrieving host information: ", e);
		} finally {
			String[] hostInformations = hostInformation.split("@");
			this.processValues.updatePid(Integer.parseInt(hostInformations[0]));
			this.processValues.updateHostname(hostInformations[1]);
		}
	}
}
