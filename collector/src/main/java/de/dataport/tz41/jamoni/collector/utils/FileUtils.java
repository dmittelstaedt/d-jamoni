package de.dataport.tz41.jamoni.collector.utils;

import java.io.File;
import java.io.IOException;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class FileUtils {
	
	public static boolean isDirectory(String directoryName) {
		File file = new File(directoryName);
		return file.exists() && file.isDirectory();
	}
	
	public static String getDirectoryFromPath(String path) throws IOException {
		File file = new File(path);
		File fileCanonical = file.getCanonicalFile();
		return fileCanonical.getParent();
	}

}
