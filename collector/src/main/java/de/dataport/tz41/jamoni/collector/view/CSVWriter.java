package de.dataport.tz41.jamoni.collector.view;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import de.dataport.tz41.jamoni.collector.model.ProcessValues;

/**
 * 
 * @author David Mittelstaedt
 *
 */
public class CSVWriter implements PropertyChangeListener{

	private static final Logger logger = LogManager.getLogger(CSVWriter.class);

	private static final String dateFormat = "yyyy-MM-dd HH:mm:ss.S";
	private static final String dateFormatFile = "yyyy-MM-dd";

	private String dataPrefix;
	private ProcessValues processValues;

	public CSVWriter(String dataPrefix, ProcessValues processValues) {
		this.dataPrefix = dataPrefix;
		this.processValues = processValues;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		try {
			String csvFile = this.dataPrefix + "-" + new SimpleDateFormat(dateFormatFile).format(Calendar.getInstance().getTime()) + ".csv";
			File file = new File(csvFile);
			boolean isFile = (file.exists() && file.isFile());
			CSVPrinter csvPrinter = null;
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			if (isFile) {
				csvPrinter = new CSVPrinter(bufferedWriter, CSVFormat.DEFAULT);
			} else {
				csvPrinter = new CSVPrinter(bufferedWriter, CSVFormat.DEFAULT.withHeader("Date", "Type", "Value"));
			}
			if (evt.getPropertyName().equals("date")) {
				csvPrinter.printRecord(new SimpleDateFormat(dateFormat).format(evt.getNewValue()), this.processValues.getType(), this.processValues.getHeap());
			} else {
				csvPrinter.printRecord(new SimpleDateFormat(dateFormat).format(this.processValues.getDate()), this.processValues.getType(), evt.getNewValue());
			}
			csvPrinter.flush();
			csvPrinter.close();

		} catch (IOException e) {
			logger.error("Error while writing to data file: " + e.getMessage());
			logger.debug("Error while writing to data file: ", e);
		}
	}

}
