#!/usr/bin/env bash
#
# Start, stops and monitors d-jamoni-collector
#
# Requires one argument (start|stop)
#
# Author: David Mittelstaedt <david.mittelstaedt@dataport.de>
# Date: 2019-01-03
VERSION="CurrentVersion"

RC=1

CURRENT_DIR="$( cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd )"

LOG_FILE=$(grep "name=\"file\" value" ${CURRENT_DIR}/custom/log4j2.xml | cut -d = -f 3 | cut -d \" -f 2)

case "$1" in
  start)
    nohup java -jar -Duser.dir=${CURRENT_DIR} -Dlog4j.configuration=file:${CURRENT_DIR}/custom/log4j2.xml \
      ${CURRENT_DIR}/libs/d-jamoni-collector-${VERSION}.jar >${LOG_FILE} 2>&1 &
    RC=$?
  ;;
  stop)
    pid=$(ps -efww | grep -v grep | grep "${CURRENT_DIR}" | grep "d-jamoni-collector-${VERSION}.jar" | awk '{print $2}')
    if [ ! -z "${pid}" ]; then
      kill -15 ${pid}
      RC=$?
    fi
  ;;
esac

exit $RC
