# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- separate Dockerfile for WildFly instance and collector
- Save heap size and daily import in web application to analyze (period, init, max configured, min, max, avg)
- Which memory information are important (committed, non heap, threads, CPU)
- Tests to verify functionality
- Add option to monitor normal Java process
- Exception Handling if file to log not exists or is accessible
- Add possibility to load other properties file
- Handle situations when Java process is not accessible --> e.g all values 0 or -1

### Changed
- Exception Handling (Remove catch and throw)
### Deprecated
### Removed
### Fixed
### Security

## [0.0.1] - 2018-12-20
### Added
- Read init, Heap and max memory from WildFly
- Write information to CSV file
- Properties file for configuring monitoring (target ip, target port, interval, output file) with default values
- Observer pattern for monitoring heap size --> enables different observers (heap size as class variable)
- Ensure WildFly is listening, otherwise throw exception and terminate
- Write to daily data file
- Write error and debug logs to file via log4j2.xml
- Ensure that configuration file exists --> handle FileNotFoundException (RC=1)
- Package collector to zip file with custom directory and finally upload
- Write bash script to start monitoring
- Add reconnect for restarted WildFlys instances

[Unreleased]: https://gitlab.com/dmittelstaedt/d-jamoni
[0.0.1]: https://gitlab.com/dmittelstaedt/d-jamoni/tree/v0.0.1
